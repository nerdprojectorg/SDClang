This is the release of Snapdragon LLVM ARM C/C++ compiler toolchain version
16.0. This is a major revision update of the toolchain. 

The 16.0 release is available for the following platforms:

1. Linux x64 host running Ubuntu 14.04 or later.
2. Windows x64 host running Windows 7 or later.
3. Linux ARM64 host running Ubuntu 14.04 or later.
4. Windows ARM64 host running Windows 10 or later.

The Snapdragon LLVM ARM Compiler will generate code for ARM and X86 targets and
will not generate code for any of the other targets supported by the llvm.org
compiler.

The Snapdragon LLVM ARM 16.0 toolchain includes all changes in the
llvm.org 15.0 release branch, and proprietary features and optimizations
that are not available in the llvm.org 15.0 branch.

The Snapdragon LLVM ARM Compiler uses the integrated assembler for assembling
and includes a full featured proprietary linker (QC Linker) for linking.

The following link contains all the new features in the llvm.org 15.0 release.
https://discourse.llvm.org/t/llvm-15-0-0-release/65099

The SDLLVM 16.0 release enables Position Independent Code (PIE) generation by 
default for Linux target triples. This feature may increase code size in some
cases and can be disabled by passing -fno-PIE option to the clang driver during
compilation and -no-pie option to the clang driver or linker during linking. If
your code base currently does not enable PIE, you need to explicitly disable it
using the above options.

The Snapdragon LLVM ARM 16.0 toolchain contains all the above features,
including the Windows ARM64 support compared to the Snapdragon LLVM ARM 14.1.5
toolchain.

The 16.0 release includes all the features in SDLLVM 14.1.5 release as well as
additional changes from llvm.org 15.0 branch. However, the 16.0 release does
not support advanced pointer authentication feature available in 14.0.3
release. The 16.0 release contains support for basic pointer authentication
features available in llvm.org 15.0 branch.  Code bases relying on advanced
pointer authentication feature should contact your customer support 
representative.

For more details about these features, please refer to section 4.3.17 of the
SDLLVM user guide or contact your customer support representative.


Documentation
The complete user guides for this Snapdragon LLVM ARM compiler and linker
are available in the 'documents' directory at the root of the SDLLVM
toolchain, which was downloaded and installed by the Qualcomm Package
Manager (QPM).

Contacts & Bug Reporting
Please post all questions about this release to
http://developer.qualcomm.com/llvm-forum

